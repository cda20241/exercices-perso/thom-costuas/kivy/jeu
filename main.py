# main.py
from kivy.app import App
from UserController import UserController

class MyApp(App):
    """
    Represents the main application class.

    Attributes:
        controller (UserController): The controller instance for managing user interactions.

    Methods:
        build(): Initializes and returns the application interface.

    Usage:
        app = MyApp()
        app.run()
    """

    def build(self):
        """
        Initializes the application by creating a UserController instance and returning its associated view.
        """
        self.controller = UserController()
        return self.controller.view

if __name__ == "__main__":
    MyApp().run()
