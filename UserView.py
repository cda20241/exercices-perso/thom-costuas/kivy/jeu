# UserView.py
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput

class UserView(BoxLayout):
    """
    Represents the view for user data input.

    Attributes:
        controller: The associated controller managing user interactions.

    Methods:
        __init__(): Initializes a new instance of the UserView class.
        on_submit(): Handles the submit button press event.
        update_label(): Updates the label with user data.

    Usage:
        user_view = UserView(controller_instance)
    """

    def __init__(self, controller, **kwargs):
        """
        Initializes a new instance of the UserView class with input fields and a submit button.
        """
        super(UserView, self).__init__(**kwargs)
        self.controller = controller

        self.orientation = "vertical"

        self.label = Label(text="Utilisateur : "+self.controller.model.prenom+" "+self.controller.model.nom)
        self.input_prenom = TextInput()
        self.input_nom = TextInput()
        self.submit_button = Button(text="Valider", on_press=self.on_submit)

        self.add_widget(self.label)
        self.add_widget(self.input_prenom)
        self.add_widget(self.input_nom)
        self.add_widget(self.submit_button)

    def on_submit(self, instance):
        """
        Handles the submit button press event.
        Notifies the controller with the entered user data.
        """
        self.controller.update_data(self.input_prenom.text, self.input_nom.text)

    def update_label(self, prenom, nom):
        """
        Updates the label with the provided user data.
        """
        self.label.text = "Utilisateur : " + prenom + " " + nom
