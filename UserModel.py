# UserModel.py
class UserModel:
    """
    Represents the model for user data.

    Attributes:
        nom (str): The last name of the user.
        prenom (str): The first name of the user.

    Methods:
        __init__(): Initializes a new instance of the UserModel class.

    Usage:
        user = UserModel()
    """

    def __init__(self):
        """
        Initializes a new instance of the UserModel class with default empty values for 'nom' and 'prenom'.
        """
        self.nom = ""
        self.prenom = ""
