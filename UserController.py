# UserController.py
from PersonnageController import PersonnageController
from PersonnageView import PersonnageView
from UserModel import UserModel
from UserView import UserView

class UserController:
    """
    Represents the controller for managing user interactions.

    Attributes:
        model (UserModel): The model instance for user data.
        view (UserView): The associated view for user data input.
        personnage_view (PersonnageView): The view for character representation.

    Methods:
        __init__(): Initializes a new instance of the UserController class.
        update_data(): Updates the user data model and view.
        switch_to_game(): Switches the view to the game window.

    Usage:
        user_controller = UserController()
    """

    def __init__(self):
        """
        Initializes a new instance of the UserController class with default model and views.
        """
        self.model = UserModel()
        self.view = UserView(self)
        self.personnage_view = PersonnageController().view

    def update_data(self, prenom, nom):
        """
        Updates the user data model and view with the provided data.
        """
        self.model.prenom = prenom
        self.model.nom = nom
        self.view.update_label(prenom, nom)
        self.switch_to_game()

    def switch_to_game(self):
        """
        Switches to the game window by replacing the current view with the character view.
        """
        self.view.clear_widgets()
        self.view.add_widget(self.personnage_view)
