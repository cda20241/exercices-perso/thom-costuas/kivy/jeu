# PersonnageController.py
from kivy.clock import Clock
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.button import Button

from PersonnageView import PersonnageView


class PersonnageController:
    """
    Represents the controller for managing character movements.

    Attributes:
        move_up_flag (bool): Flag indicating if the character is moving up.
        move_down_flag (bool): Flag indicating if the character is moving down.
        move_left_flag (bool): Flag indicating if the character is moving left.
        move_right_flag (bool): Flag indicating if the character is moving right.
        input_pressed_delay (float): Delay between consecutive input presses.
        movement_speed (int): Speed of character movement.
        view (PersonnageView): The associated view for character representation.
        character_position (list): The current position of the character.

    Methods:
        __init__(): Initializes a new instance of the PersonnageController class.
        start_move_up(): Initiates character movement upward.
        start_move_down(): Initiates character movement downward.
        start_move_left(): Initiates character movement to the left.
        start_move_right(): Initiates character movement to the right.
        stop_move(): Stops character movement.
        move_up(): Moves the character upward.
        move_down(): Moves the character downward.
        move_left(): Moves the character to the left.
        move_right(): Moves the character to the right.
        update_character_position(): Updates the character's position in the view.
    """

    def __init__(self, **kwargs):
        # Movement flags
        self.move_up_flag = False
        self.move_down_flag = False
        self.move_left_flag = False
        self.move_right_flag = False

        self.input_pressed_delay = 0.05

        # Movement speed
        self.movement_speed = 5

        self.view = PersonnageView(self)

        # Initial character position
        self.character_position = [0, 0]

    def start_move_up(self, instance):
        self.move_up_flag = True
        Clock.schedule_interval(self.move_up, self.input_pressed_delay)

    def start_move_down(self, instance):
        self.move_down_flag = True
        Clock.schedule_interval(self.move_down, self.input_pressed_delay)

    def start_move_left(self, instance):
        self.move_left_flag = True
        Clock.schedule_interval(self.move_left, self.input_pressed_delay)

    def start_move_right(self, instance):
        self.move_right_flag = True
        Clock.schedule_interval(self.move_right, self.input_pressed_delay)

    def stop_move(self, instance):
        self.move_up_flag = False
        self.move_down_flag = False
        self.move_left_flag = False
        self.move_right_flag = False
        Clock.unschedule(self.move_up)
        Clock.unschedule(self.move_down)
        Clock.unschedule(self.move_left)
        Clock.unschedule(self.move_right)

    def move_up(self, dt):
        self.character_position[1] += self.movement_speed
        self.update_character_position()

    def move_down(self, dt):
        self.character_position[1] -= self.movement_speed
        self.update_character_position()

    def move_left(self, dt):
        self.character_position[0] -= self.movement_speed
        self.update_character_position()

    def move_right(self, dt):
        self.character_position[0] += self.movement_speed
        self.update_character_position()

    def update_character_position(self):
        self.view.character_image.pos = self.character_position
