# PersonnageView.py
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.button import Button

class PersonnageView(BoxLayout):
    """
    Represents the view for the character in the game.

    Attributes:
        controller: The associated controller managing character movements.

    Methods:
        __init__(): Initializes a new instance of the PersonnageView class.

    Usage:
        personnage_view = PersonnageView(controller_instance)
    """

    def __init__(self, controller, **kwargs):
        """
        Initializes a new instance of the PersonnageView class with directional buttons and a character image.
        """
        super(PersonnageView, self).__init__(**kwargs)
        self.controller = controller

        self.orientation = "vertical"

        # Directional buttons for character movement
        self.button_layout = GridLayout(cols=4, size_hint=(1, 0.1), spacing=10)
        self.left_button = Button(text="Left", size_hint=(0.5, 0.5), on_press=self.controller.start_move_left, on_release=self.controller.stop_move)
        self.up_button = Button(text="Up", size_hint=(0.5, 0.5), on_press=self.controller.start_move_up, on_release=self.controller.stop_move)
        self.down_button = Button(text="Down", size_hint=(0.5, 0.5), on_press=self.controller.start_move_down, on_release=self.controller.stop_move)
        self.right_button = Button(text="Right", size_hint=(0.5, 0.5), on_press=self.controller.start_move_right, on_release=self.controller.stop_move)

        self.button_layout.add_widget(self.left_button)
        self.button_layout.add_widget(self.up_button)
        self.button_layout.add_widget(self.down_button)
        self.button_layout.add_widget(self.right_button)

        self.add_widget(self.button_layout)

        # Image layout
        self.image_layout = GridLayout(cols=1, size_hint=(1, 0.5), spacing=10)
        self.character_image = Image(source="character.jpg", size_hint=(None, None), size=(50, 50), allow_stretch=True)

        self.image_layout.add_widget(self.character_image)

        self.add_widget(self.image_layout)
